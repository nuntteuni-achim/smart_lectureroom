var mongoDB = require('../../dao/student_web/mongoDB');
var rekognition = require('../../api/aws_rekognition/compareImg');
var fcm = require('../../api/fcm/requestPush');
var imgSource = require('./imgSource');
var atob = require('atob');
var fs = require('fs');

module.exports = {

    registerProcess : (userInfo, callback)=>{
        const student_id = userInfo.student_id;
        const student_pw = userInfo.student_password;
        const student_name = userInfo.student_name;
        const queryObject = {"student_id":{"$in":[student_id]}}; //몽고디비 쿼리 내용

        mongoDB
        .findStudent(queryObject)
        .then((docsPack)=>{
            const docs = docsPack.docs;
            const amount = docs.length;
            let result = {
                STATE : "SUCCESS",
                DETAIL : "SUCCESS_REGISTER"
            }

            if(amount===1){
                result.STATE = "ERR";
                result.DETAIL = "ALEADY_REGISTERED_ID";
            }

            if(result.STATE ==="SUCCESS"){
                let insertObject={
                    student_id :student_id,
                    student_pw :student_pw,
                    student_name : student_name
                }; //insert내용을 정의해주기

                mongoDB.insertStudent(insertObject)
                .then(()=>{
                    callback(result);
                });
            }
            else{
                callback(result);
            }


        })
    },

    loginProcess : (userInfo, callback)=>{
        const id = userInfo.student_id;
        const pw = userInfo.student_password;
        console.log("-----------------");
        console.log("loginProcess for Android(Student)")
        console.log("id : " + id);
        console.log("pw : " + pw);
        console.log("-----------------");

        if(id==""){ //아이디를 입력하지 않음
            callback({STATE :"ERR" , DETAIL:"EMPTY_ID"});
            return;
        }
        if(pw==""){//비밀번호를 입력하지 않음
            callback({STATE :"ERR" , DETAIL:"EMPTY_PASSWORD"});
            return;
        }

        const queryObject = {"student_id":{"$in":[id]}}; //몽고디비 쿼리 내용
        mongoDB.findStudent(queryObject)
        .then((docsPack)=>{
            const docs = docsPack.docs;
            const amount = docs.length;

            let result={
                STATE : "SUCCESS",
                DETAIL : "SUCCESS_LOGIN",
                STUDENTNAME : docs[0].student_name
            };
            if(amount===1){ //회원가입된 아이디
                const student = docs[0];
                if(student.student_password!==pw){ //비밀번호가 일치하지않음
                    result.STATE="ERR";
                    result.DETAIL="NOT_CORRECT_PASSWORD";
                }
                else{ //아이디와 비밀번호 모두 일치
                    result.data ={
                        id : student.student_id, //로그인 id 저장
                        name : student.student_name //로그인 성명 저장
                    }
                }
            }
            if(amount===0){ //회원가입되지 않은 아이디
                result.STATE="ERR";
                result.DETAIL="NOT_FOUND_ID";
            }


            callback(result);
        })
        .catch((err)=>{console.log(err);});
    },

    changePasswordProcess : (userInfo, callback)=>{
        const id = userInfo.student_id;
        const pw = userInfo.student_password;

        console.log("-----------------");
        console.log("loginProcess for Android(Student)")
        console.log("id : " + id);
        console.log("pw : " + pw);
        console.log("-----------------");

        const queryObject = {"student_id":{"$in":[id]}}; //몽고디비 쿼리 내용
        mongoDB.findStudent(queryObject)
        .then((docsPack)=>{
            const docs = docsPack.docs;
            const amount = docs.length;
            let result={
                STATE : "SUCCESS",
                DETAIL : "SUCCESS_LOGIN"
            };
            if(amount===1){ //회원가입된 아이디
                const student = docs[0];
                if(result.STATE ==="SUCCESS"){
                    let updateObject = {
                         query : {student_id: id},
                         update : { $set: { student_password:pw}}
                    }

                    result.data ={
                        id : student.student_id, //로그인 id 저장
                        name : student.student_name //로그인 성명 저장
                    }

                    mongoDB.updateStudent(updateObject)
                    .then(()=>{
                        callback(result);
                    });
                }
            }
            if(amount===0){ //회원가입되지 않은 아이디
                result.STATE="ERR";
                result.DETAIL="NOT_FOUND_ID";
            }else{
              callback(result);
            }
        })
        .catch((err)=>{console.log(err);});
    },

    getTokenProcess : (userInfo, callback)=>{
        const id = userInfo.student_id;
        const token = userInfo.student_token;

        console.log("-----------------");
        console.log("loginProcess for Android(Student)")
        console.log("id : " + id);
        console.log("토큰 : " + token);
        console.log("-----------------");

        const queryObject = {"student_id":{"$in":[id]}}; //몽고디비 쿼리 내용
        mongoDB.findStudent(queryObject)
        .then((docsPack)=>{
            const docs = docsPack.docs;
            const amount = docs.length;
            let result={
                STATE : "SUCCESS",
                DETAIL : "SUCCESS_LOGIN"
            };
            if(amount===1){ //회원가입된 아이디
                const student = docs[0];
                if(result.STATE ==="SUCCESS"){
                    let updateObject = {
                         query : {student_id: id},
                         update : { $set: { student_token: token}}
                    }

                    result.data ={
                        id : student.student_id, //로그인 id 저장
                        name : student.student_name //로그인 성명 저장
                    }

                    mongoDB.updateStudent(updateObject)
                    .then(()=>{
                        callback(result);
                    });
                }
            }
            if(amount===0){ //회원가입되지 않은 아이디
                result.STATE="ERR";
                result.DETAIL="NOT_FOUND_ID";
            }else{
              callback(result);
            }
        })
        .catch((err)=>{console.log(err);});
    },

    getScheduleProcess : (userInfo, callback)=>{
        const id = userInfo.student_id;
        var info_temp=[];
        var testList = [] ;
        var data = new Object() ;

        const queryObject = {"student_id":{"$in":[id]}}; //몽고디비 쿼리 내용
        mongoDB.getLectureInfo_student(queryObject)
        .then((content)=>{
          const lecture = content.lecture;
          for(let j=0; j<lecture.length; j++){
            for(let k=0; k<lecture[j].lecture_info.length; k++){
                let lecture_id = lecture[j].lecture_id; //강의id
                let lecture_name = lecture[j].lecture_name; //강의명

                let lecture_info = lecture[j].lecture_info[k]; //강의정보
                let day = lecture_info.lecture_time.substr(0,1); // ex) 월2 -> 월  , 2
                let time = lecture_info.lecture_time.substr(1,1);

                let lecture_room = lecture_info.lectureroom; //강의장소
                let building = lecture_room.building_name+" "+lecture_room.lectureroom_num;

                if(day=="월"){
                  day=1;
                }else if(day=="화"){
                  day=2;
                }else if(day=="수"){
                  day=3;
                }else if(day=="목"){
                  day=4;
                }else if(day=="금"){
                  day=5;
                }
                info_temp.push("a"+day+","+time+","+lecture_name+","+building+","+lecture_id+"a");
              }
            }
            // console.log(info_temp);

            // var jsonData = JSON.stringify(info_temp) ;
            // console.log(jsonData);

            let result={
                STATE : "SUCCESS",
                DETAIL : info_temp

                /*
                //DETAIL : content //쿼리내용 끌고갑니다아아
                {"lecture":
                  [
                    {"lecture_name":"알고리즘","lecture_id":"1",
                    "lecture_info":[
                        {"lectureroom_id":"1","lecture_time":"목8","lectureroom":{"building_name":"실습관","lectureroom_num":"411호","camera_id":"DDIE123"}},
                        {"lectureroom_id":"1","lecture_time":"목9","lectureroom":{"building_name":"실습관","lectureroom_num":"411호","camera_id":"DDIE123"}},
                        {"lectureroom_id":"2","lecture_time":"월2","lectureroom":{"building_name":"실습관","lectureroom_num":"410호","camera_id":"DDIE120"}}
                      ]
                    },

                    {"lecture_name":"자료구조","lecture_id":"2",
                    "lecture_info":[
                      {"lectureroom_id":"2","lecture_time":"화5","lectureroom":{"building_name":"실습관","lectureroom_num":"410호","camera_id":"DDIE120"}},
                      {"lectureroom_id":"2","lecture_time":"화6","lectureroom":{"building_name":"실습관","lectureroom_num":"410호","camera_id":"DDIE120"}},
                      {"lectureroom_id":"2","lecture_time":"화7","lectureroom":{"building_name":"실습관","lectureroom_num":"410호","camera_id":"DDIE120"}}
                    ]
                  },

                    {"lecture_name":"자바스크립트","lecture_id":"3",
                    "lecture_info":[
                      {"lectureroom_id":"1","lecture_time":"목1","lectureroom":{"building_name":"실습관","lectureroom_num":"411호","camera_id":"DDIE123"}},
                      {"lectureroom_id":"1","lecture_time":"목2","lectureroom":{"building_name":"실습관","lectureroom_num":"411호","camera_id":"DDIE123"}},
                      {"lectureroom_id":"1","lecture_time":"목3","lectureroom":{"building_name":"실습관","lectureroom_num":"411호","camera_id":"DDIE123"}}
                    ]
                  }
                ]

                */
            };

            callback(result);
        })
        .catch((err)=>{console.log(err);});
    },

    getAttendStateProcess : (userInfo, callback)=>{
        const id = userInfo.student_id;
        var info_temp=[], testList = [];
        var data = new Object() ;

        const queryObject = {"student_id":{"$in":[id]}}; //몽고디비 쿼리 내용
        mongoDB.getAttendState(queryObject)
        .then((content)=>{
            const attend = content.attend;

            for(let i=0; i<attend.length; i++){
              let lecture_id = attend[i].lecture_id;
              let lecture_session = attend[i].lecture_session;
              let attend_state = attend[i].attend_state;
              let lecture_name = attend[i].lecture_name;
              for(let j=0; j<lecture_id.length; j++){
                info_temp.push("a"+lecture_id[j]+","+lecture_session[j]+","+attend_state[j]+","+lecture_name[j]+"a");
              }
            }

            console.log("999999999999999999999999"+info_temp)

            let result={
                STATE : "SUCCESS",
                DETAIL : info_temp
            };

            callback(result);
        })
        .catch((err)=>{console.log(err);});
    },


    /*
     * let content = {
     *      requestType : 안드로이드에서 보내줘야함(attendRequest , attendReRequest)
     *      attend_id : attend_id,
            student_token : student_token,
            student_id : student_id,
            attend_state : attend_state,
            lecture_id : lecture_id,
            lecture_name : lecture_name,
            lecture_session : lecture_session,
            reRequestLimit : 3*60*1000 //timestamp값으로 3분

            late : lectureInfo.late //지각처리를 고려해야하는 경우에 쓰이는 값
            attendStartTime : lectureInfo.attendStartTime;
        }
     */

    pushResponseProcess : (attendInfo , callback)=>{

        //테스트를 위한 더미 값
        attendInfo = {
            requestType : "attendReRequest", //attendRequest(출석요청), attendReRequest(출석재요청:이의신청)  두종류 존재
            attend_id : "53",
            student_token : "fHE0wfUUNfM:APA91bFMTxYeK1XcJVzFwy43miBXdD-q837nBqteKdP1n1_O5u73Nkl5vqn9pIjq3X7xRYlCm2YQHfjqxXGvKHxQ0adL63pCvOHyNOpuQ1uWiCiJOc6QErazZEijaJrKz_4Qz7bzHx7W",
            student_id : "2014335066",
            attend_state : "A003",
            lecture_id : "3",
            lecture_name : "자바스크립트",
            lecture_session : "1",
            reRequestLimit : new Date().getTime() + 3*60*1000, //timestamp값으로 3분

            late : "10", //지각처리를 고려해야하는 경우에 쓰이는 값
            attendStartTime : new Date().getTime()
        }

        const late = attendInfo.late; //지각허용시간(분)
        const requestType = attendInfo.requestType;
        const student_id = attendInfo.student_id;
        const attendStartTime = attendInfo.attendStartTime; //출석시작시간
        const lateLimit = attendStartTime+late*1000; //late 변수와 출석시간시작을 고려해 지각제한 시간을 저장(밀리세컨즈)
        const nowTime = new Date().getTime(); //현재시간
        const attend_id = attendInfo.attend_id;
        const lecture_session = attendInfo.lecture_session;
        const lecture_id = attendInfo.lecture_id;
        const student_token = attendInfo.student_token;
        const lecture_name = attendInfo.lecture_name;
        const reRequestLimit = attendInfo.reRequestLimit;
        /**
         * 푸시요청에는 재요청과 요청 두가지 타입이 존재함 이에 따라 세부 로직이 변경되어야함
         *
         * 1. 사진을 찍고 다찍히면 callback() ---> 즉, 사진이 다찍혔으니깐 할꺼하라고 알리는 거임
         * 2. 찍힌 사진 이용해서 얼굴비교를 하고 결과를 리턴 (이 과정에서 )
         * 3. 결과를 DB에 반영
         * 4. 결과에 따라 다른 내용을 푸시로 보내야함
         *
         *
         */
        console.log("푸시요청 테스트 -----------------------------------------")
        if(requestType ==="attendRequest"){ //출석요청인경우

            if(late){//지각처리까지해야하는 자동출석


                console.log("requestType : " + requestType);
                console.log("late : " + late);
                console.log("student_id : " + student_id);
                console.log("attendStartTime : " + attendStartTime);
                console.log("lateLimit : " + lateLimit);
                console.log("nowTime : " + nowTime);

                //결과통보(푸시알람)



                if(nowTime < lateLimit ){   //지각이라면
                    setTimeout(function(){ //사진촬영

                        /**
                         * callback을 통해 사진촬영이 완료됬다는 것을 알려야함 , 안드로이드쪽이랑 협의 필요
                         */
                        callback("사진촬영을 완료함");



                        // const img = imgSource.cameraImg; //태준,지우 없는 사진임 ,스트리밍서버에서 img 받았다는 가정....
                        const img = imgSource.groupImage2; //전부다 있는 사진임
                        findStudentFace(student_id,img).then((state)=>{
                            if(state){ //학생을 발견했다면
                                //db에서 해당학생을 지각으로 바꿔야함

                                /**
                                 * 몽고DB 쿼리 내용 :
                                 * attend_info안에 있는 객체의 내용중 attend_id 값이 일치하고
                                 * 그 객체의 attendence 배열의 객체중 lecture_session, student_id 값이 일치하는 것을 찾아서
                                 * 값을 수정하는 내용
                                 */
                                const updateObject ={
                                    query :  {
                                        $and:[
                                            {
                                                "attend_info":{
                                                    $elemMatch : {
                                                        "attend_id" : attend_id,
                                                        "attendence":{
                                                            $elemMatch : {
                                                                "lecture_session" :lecture_session,
                                                                "student_id" : student_id
                                                            }
                                                        }
                                                    }
                                                }

                                            },

                                            {
                                                "lecture_id" : lecture_id
                                            }
                                        ]
                                    },
                                    update :  {
                                        $set:{
                                            "attend_info.$[outer].attendence.$[inner]": {
                                                "student_id" : student_id,
                                                "attend_state":"A002", //지각상태로 변경
                                                "lecture_session" :lecture_session
                                            }
                                        }
                                    },
                                    arrayFilters : {
                                        "arrayFilters" : [
                                            {"outer.attend_id" : attend_id} ,
                                            {
                                                "inner.student_id" : student_id,
                                                "inner.lecture_session":lecture_session
                                            }
                                        ]
                                    }

                                }
                                mongoDB.updateStudentAttend(updateObject).then(()=>{
                                    //결과 푸시를 보내자, 너 지각처리됬어 ^^
                                    const to = student_token;
                                    const title = lecture_name + " 수업" + " \"결석 -> 지각\" 처리 되었습니다.";
                                    const body = "";
                                    const content = {};

                                    fcm.requestPush(to,title,body,content,()=>{
                                        // callback("지각처리 푸시를 보냈음");
                                        console.log(student_id + " : 결석->지각 처리")
                                    });


                                });
                            }
                            else{ //학생을 찾지 못함
                                //"너 없어, 결석이야" 라는 내용의 푸시
                                const to = student_token;
                                const title = lecture_name + " 수업" + " \"결석 -> 결석\" 처리 되었습니다.";
                                const body = "사유 : 카메라가 대상 학생을 찾지 못하였습니다.";
                                const content = {};

                                fcm.requestPush(to,title,body,content,()=>{
                                    // callback("지각처리 푸시를 보냈음");
                                    console.log(student_id + " : 결석->결석 처리A , 학생을 찾지못함")
                                });
                            }
                        });
                    },3000);
                }
                else{ //지각시간을 지난경우
                    const to = student_token;
                    const title = lecture_name + " 수업" + " \"결석 -> 결석\" 처리 되었습니다.";
                    const body = "사유 : 이미 지각허용 시간을 초과했습니다.";
                    const content = {};

                    fcm.requestPush(to,title,body,content,()=>{
                        // callback("지각처리 푸시를 보냈음");
                        console.log(student_id + " : 결석->결석 처리 , 지각허용시간 초과")
                    });
                    // callback("지각허용시간을 지났음 푸시를 보냈음");
                }
            }
            else{
                console.log("------오류발생---------------------");
                console.log("내용 : 서버에 와서는 안되는 요청이 도달함");
                //교수님이 반자동으로 직접 출석하는 경우 -----> 이경우는 아예 버튼이 없는 푸시알람이 학생에게 전달되기때문에 분기할수 없는 경우임
                //여기가 읽힌다면 안드로이드쪽 코드를 봐야함, 푸시에 버튼이 없어야해
            }

        }
        else if(requestType==="attendReRequest"){ /* 출석 재요청(이의신청) */
            console.log("테스트------리미트 값들-----");
            console.log("reRequestLimit : " + reRequestLimit);
            console.log("nowTime : " + nowTime);
            if(reRequestLimit > nowTime){ //재요청 제한시간보다 빨리 보냈다면

                setTimeout(function(){

                    /**
                     * 사진촬영이 완료되면 안드로이드쪽에게 알려야함
                     */
                    callback("사진촬영을 완료함");

                    // const img = imgSource.cameraImg; //태준,지우 없는 사진임 ,스트리밍서버에서 img 받았다는 가정....
                    const img = imgSource.groupImage2; //전부다 있는 사진임
                    findStudentFace(student_id,img).then((state)=>{
                        if(state){ //학생을 발견했다면
                            //db에서 해당학생을 지각으로 바꿔야함
                            const updateObject ={ //
                                query :  {
                                    $and:[
                                        {
                                            "attend_info":{
                                                $elemMatch : {
                                                    "attend_id" : attend_id,
                                                    "attendence":{
                                                        $elemMatch : {
                                                            "lecture_session" :lecture_session,
                                                            "student_id" : student_id
                                                        }
                                                    }
                                                }
                                            }

                                        },

                                        {
                                            "lecture_id" : lecture_id
                                        }
                                    ]
                                },
                                update :  {
                                    $set:{
                                        "attend_info.$[outer].attendence.$[inner]": {
                                            "student_id" : student_id,
                                            "attend_state":"A001", //출석상태로 변경
                                            "lecture_session" :lecture_session
                                        }
                                    }
                                },
                                arrayFilters : {
                                    "arrayFilters" : [
                                        {"outer.attend_id" : attend_id} ,
                                        {
                                            "inner.student_id" : student_id,
                                            "inner.lecture_session":lecture_session
                                        }
                                    ]
                                }

                            }
                            mongoDB.updateStudentAttend(updateObject).then(()=>{
                                //결과 푸시를 보내자, 너 지각처리됬어 ^^
                                const to = student_token;
                                const title = lecture_name + " 수업" + " \"결석 -> 출석\" 처리 되었습니다.";
                                const body = "";
                                const content = {};

                                fcm.requestPush(to,title,body,content,()=>{
                                    // callback("지각처리 푸시를 보냈음");
                                    console.log(student_id + " : 결석->출석 처리 , 재요청");
                                });


                            });
                        }
                        else{ //학생을 찾지 못함
                            //"너 없어, 결석이야" 라는 내용의 푸시
                            const to = student_token;
                            const title = lecture_name + " 수업" + " \"결석 -> 결석\" 처리 되었습니다.";
                            const body = "사유 : 카메라가 대상 학생을 찾지 못하였습니다.";
                            const content = {};

                            fcm.requestPush(to,title,body,content,()=>{
                                console.log(student_id + " : 결석->결석 처리 , 학생을 찾지 못함");
                            });
                        }
                    });

                },3000);


            }
            else{ //재요청 이후에 보냈다면
                const to = student_token;
                const title = lecture_name + " 출석재요청(이의신청)이 거부되었습니다.";
                const body = "사유 : 재요청 허용시간 초과(출석시작 후 3분)";
                const content = {};

                fcm.requestPush(to,title,body,content,()=>{
                    // callback("재요청 거부함");
                    console.log(student_id + " : 결석->결석 처리 , 재요청(이의신청)허용시간을 초과함");
                });
                //callback("너 재요청 못함 ㅎ")
            }

        }
        else{ //requestType에 예상하지 못한 값이 저장되어있음
            console.log("----에러발생---------")
            console.log("requestType 값 오류!!")
            console.log("requestType : " + requestType);
        }

    }

}






function findStudentFace(student_id, img){
    return new Promise((resolve, reject)=>{
        fs.readFile('./resources/images/student/'+student_id+'.jpg' ,function(error, data) {
            console.log('err : ' + error);
            let img1 = getBinary(data);  //학생 한명의 이미지
            let img2 = getBinary(img);// 강의실 촬영 이미지
            // let img2 = getBinary(imgSource.groupImage2);// 강의실 촬영 이미지

            let state = false;//학생이 강의실에 있는지 없는지를 나타냄
            rekognition.compareFacesByte(img1, img2,function(err, data){
                if (err) console.log(err, err.stack); // an error occurred
                else     console.log(data);           // successful response
                const sourceImageFace = data;
                const faceMatches = sourceImageFace.FaceMatches;

                for(let i=0; i<faceMatches.length; i++){
                    if(faceMatches[i].Similarity > 95.0){ //유사도가 95% 이상이라면
                        state = true;
                    }
                }
                resolve(state);
            });
        });
    })


}

function getBinary(base64Image) {
    var binaryImg = atob(base64Image); //64바이너리로 디코딩하는 함수
    var length = binaryImg.length;
    var ab = new ArrayBuffer(length);
    var ua = new Uint8Array(ab);
    for (var i = 0; i < length; i++) {
      ua[i] = binaryImg.charCodeAt(i);
    }

    return ab;
}
