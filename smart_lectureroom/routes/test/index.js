var express = require('express');
var router = express.Router();
var mongoDB_professor = require('../../extend_modules/dao/professor_web/mongoDB');
var rekognition = require('../../extend_modules/api/aws_rekognition/compareImg');
var atob = require('atob');
/* GET home page. */
router.get('/test1', function(req, res, next) {
  const img1 = req.
  res.render('test/page/test1', { 
    pageName: 'test1'
  });
});

router.post('/process/test1', function(req, res, next) {
  try{
    let img1 = getBinary(req.body.img1);  
    let img2 = getBinary(req.body.img2);
    console.log("테스트@@@@@@@@@@@@@");
    // console.log(img1.substr(0,10));

    rekognition.compareFacesByte(img1, img2);
    res.send(
      {
        result : {STATE : "SUCCESS" , DETAIL : "TEST"},
      }
    );    
  }
  catch(err){
    console.log(err);
  };  
});

router.get('/test2', function(req, res, next) {
  mongoDB_professor.getSequence("attend_id")
  .then((results)=>{
    console.log("테스트----------------");
    console.log(JSON.stringify(results));
    res.send({result:results});    
  })
  .catch((err)=>{console.log(err)});
});


function getBinary(base64Image) {
  var binaryImg = atob(base64Image); //64바이너리로 디코딩하는 함수
  var length = binaryImg.length;
  var ab = new ArrayBuffer(length);
  var ua = new Uint8Array(ab);
  for (var i = 0; i < length; i++) {
    ua[i] = binaryImg.charCodeAt(i);
  }

  return ab;
}

module.exports = router;
