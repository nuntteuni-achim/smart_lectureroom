package com.nuntteuniachim.sroomi.view.setting

interface SettingContract {

    interface View {

    }

    interface Presenter {
        fun setView(view: SettingContract.View?)
        fun releaseView()

        fun registerPictures()
    }
}
