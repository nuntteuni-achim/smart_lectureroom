package com.nuntteuniachim.sroomi.view.setting

import android.app.AlertDialog
import com.nuntteuniachim.sroomi.R
import com.nuntteuniachim.sroomi.model.IMyService
import com.nuntteuniachim.sroomi.model.RetrofitClient
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

open class SettingPresenter internal constructor() : SettingContract.Presenter {
    private var view: SettingContract.View? = null

    private var disposable: CompositeDisposable? = CompositeDisposable()
    private var retrofitClient = RetrofitClient.getInstance()
    private var iMyService: IMyService? = (retrofitClient as Retrofit).create(IMyService::class.java)

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    override fun setView(view: SettingContract.View?) {
        this.view = view
    }

    override fun releaseView() {
        disposable?.clear()
    }

    override fun registerPictures(){
        val builder = AlertDialog.Builder(SettingActivity.mContext)
        builder.setTitle("사진 등록/수정")

        builder.setItems(R.array.LAN) { dialog, pos ->
            when (pos) {
//                0 -> takePhoto()
//                1 -> goAlbum()
            }
        }

        val alertDialog = builder.create()
        alertDialog.show()
    }
}
