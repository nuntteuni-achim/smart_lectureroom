package com.nuntteuniachim.sroomi.view.main

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import android.widget.Toast
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.nuntteuniachim.sroomi.base.SharedPreferenceManager
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.nuntteuniachim.sroomi.AttendRequestActivity
import com.nuntteuniachim.sroomi.R
import com.nuntteuniachim.sroomi.base.FragmentStatePagerAdapter
import kotlinx.android.synthetic.main.activity_fragment.*
import kotlinx.android.synthetic.main.fragment_home.*

class FragmentActivity : AppCompatActivity(), FragmentContract.View, View.OnClickListener {
    private val presenter = FragmentPresenter()
    private val TOPIC = "send" //fcm firebase 토픽 선언
    private val fragmentAdapter: FragmentStatePagerAdapter by lazy { FragmentStatePagerAdapter(3, supportFragmentManager) }

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var mContext: Context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment)
        mContext = this

        presenter.setView(this) // presenter 연결

        btn_homefragment.setOnClickListener(this) //리스너 연결
        btn_schedulefragment.setOnClickListener(this)
        btn_third.setOnClickListener(this)

        val viewpager = findViewById<View>(R.id.fragment_container) as ViewPager
        presenter.isFirst(fragmentAdapter, viewpager)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_homefragment -> fragment_container.currentItem = 0
            R.id.btn_schedulefragment -> fragment_container.currentItem = 1
            R.id.btn_third -> fragment_container.currentItem = 2
        }
    }

    override fun getToken() {
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this) { instanceIdResult ->
            val token = instanceIdResult.token  //토큰 생성
            SharedPreferenceManager.setString(mContext, "PREFTOKEN", token)
        }
        FirebaseMessaging.getInstance().subscribeToTopic(TOPIC)  //알림허용
    }

    //뒤로가기 버튼을 두번 연속으로 눌러야 종료
    var time: Long = 0
    override fun onBackPressed() {
        if (System.currentTimeMillis() - time >= 2000) {
            time = System.currentTimeMillis();
            Toast.makeText(applicationContext, "뒤로 버튼을 한번 더 누르면 종료합니다.", Toast.LENGTH_SHORT).show();
        } else if (System.currentTimeMillis() - time < 2000) {
            finish()
        }
    }

    override fun onStop() {
        super.onStop()
        presenter.releaseView() // presenter 연결 해제
    }
}
